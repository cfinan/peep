"""
A selection of file handlers for navigating files and providing content
"""
from peep import constants as con
from collections import deque
import numpy as np
import csv


# @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
class CsvFileHandler(object):
    """
    Implements access and buffing of lines from a delimited file
    """

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def __init__(self, fn, **kwargs):
        """
        Initialise

        fn :str or sys.stdin
            The input file data, could be a file name of STDIN
        init_rows :int (optional)
            The absolute minumum number of lines that should be buffered
            irrespective of memory, this equates to the minimum terminal number
            of lines, so we at least need to fill the terminal
        """
        # The amount of memory that should be used to buffer data in the file
        self.mem_buffer = kwargs.pop('mem_buffer', con.DEFAULT_BUFFER)
        self.has_header = kwargs.pop('header', con.DEFAULT_HEADER)
        self.has_row_names = kwargs.pop('row_names', con.DEFAULT_ROW_NAMES)
        init_rows = kwargs.pop('init_rows', con.DEFAULT_INIT_ROWS)

        # The logger is used for debugging as I can't echo to STDOUT
        self.logger = kwargs.pop('logger', None)
        self.log("init_rows={0}".format(init_rows))

        # Open the file
        self.open(fn)
        self.csv_kwargs = kwargs

        # Initialise object level variables
        self.memory = 0
        self.line_buffer = []
        self.max_cols = 0
        self.colwidths = np.zeros(1)
        self.seek_pos = []

        # Initialise with the number of lines to fill the terminal or the
        # memory buffer
        self.fill_buffer(init_rows)
        self.set_header()
        self.set_row_names()

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def __repr__(self):
        return self.get_details()

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def __str__(self):
        return self.get_details()

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def __enter__(self):
        """
        Entering the context manager
        """
        return self

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def __exit__(self, *exc):
        """
        Exiting the context manager
        """
        self.close()

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def close(self):
        """
        Close the reader. This closes the input file and clears the buffer
        """
        self.log("* Closing Reader *")
        self.line_buffer.clear()

        # TODO: Need to except here for closing STDIN
        self.infile.close()

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def log(self, msg, method='info'):
        """
        log a message to the logger (if it exists)
        """
        try:
            self.logger.info(msg)
            # setattr(self.logger, method, msg)
        except AttributeError:
            pass

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def open(self, fn, **kwargs):
        """
        Open the file

        Parameters
        ----------
        """
        self.infile = open(fn)
        # self.reader = csv.reader(self.infile, **kwargs)

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def get_details(self):
        """
        Printable details of the object
        """
        return "<{0}(nlines='{1}',memory='{2}MB', maxcols='{3}')>".format(
            self.__class__.__name__,
            len(self.line_buffer),
            round(self.memory/1048576, 2),
            self.max_cols)

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def fill_buffer(self, init_lines):
        """
        Fill the buffer to either the maximum memory or the minimum lines. The
        minimum lines reflect the number of lines required to fill the
        observable terminal rows. These will be filled irrespective of memory.
        Once min_lines is satisfied, then the buffer memory is filled. The
        initial lines read in will form the final size of the buffer to hold
        the data
        """
        # Log the memory buffer
        self.log("memory_buffer: {0}".format(self.mem_buffer))

        # While the memory for the buffer has not been filled then read the
        # data to fill it whilst also ensuring that we have enough lines to
        # fill the terminal
        line_buffer = []
        while self.memory < self.mem_buffer or len(line_buffer) < init_lines:
            # Get a line
            try:
                line = self.get_line()
            except StopIteration:
                break

            # Now update the max col widths
            self.set_col_widths(line)

            # Update the memory used
            self.memory += line.size * line.itemsize

            # Staore the line
            line_buffer.append(line)

        # Log the memory
        self.log("memory_used: {0}".format(self.memory))

        # Now use a deque to implement the buffer, then count the maximum
        # number of lines in the buffer 
        self.line_buffer = deque(line_buffer, len(line_buffer))
        self.log("init_max_cols={0}".format(self.max_cols))

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def set_header(self):
        """
        set the header row
        """
        self.log("has_header={0},{1}".format(self.has_header,
                                             int(self.has_header)))
        if self.has_header is True:
            self.header = self.line_buffer[0]
        else:
            self.header = np.arange(1, self.max_cols)

        # Get the character lengths of each element in the row names and then
        # get the max of them
        self.max_header_names_width = max(chr_len(self.header))

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def set_row_names(self):
        """
        set the header row
        """
        self.log("has_row_names={0},{1}".format(self.has_row_names,
                                                int(self.has_row_names)))

        # If the file has row names
        if self.has_row_names is True:
            # TODO: should catch index errors here
            self.row_names = np.array([i[0] for i in self.line_buffer])
        else:
            # If not then we create integer row names
            self.row_names = np.array(
                [str(i) for i in range(0, len(self.line_buffer))])

        # Get the character lengths of each element in the row names and then
        # get the max of them
        self.max_row_names_width = max(chr_len(self.row_names))

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def get_lines(self, nlines=1):
        """
        Get nlines from the file

        Parameters
        ----------
        nlines :int
            The number of lines to get
        """
        for i in range(nlines):
            try:
                # Get a line
                line = self.get_line()
            except StopIteration:
                pass

            self.memory += line.size * line.itemsize
            self.line_buffer.append(line)

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def get_line(self):
        """
        Get a single line from the input file. This should also update the
        max_cols attribute and raise a StopIteration when at the end of the
        file
        """
        # Get the start of the line position
        self.seek_pos.append(self.infile.tell())

        # Grab a line, this is preyy nasty and inefficient but I can't use a
        # call to next as I can't call tell() on the file handle
        # TODO: find a better way to do this - maybe have a usecsv option
        line = np.array(next(csv.reader([self.infile.readline().rstrip('\n')],
                                        **self.csv_kwargs)))

        # Update the max_cols and the update the column widths
        self.max_cols = max(self.max_cols, line.size)
        self.set_col_widths(line)

        return line

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def get_ncols(self):
        """
        Get the number of columns in the buffer, So, some files may not have
        uniform numbers of columns, so we base numbers of columns on the max
        number of columns in the buffer
        """
        if self.max_cols == 0:
            for i in self.line_buffer:
                self.max_cols = max(self.max_cols, i.size)
            self.log("init_max_cols={0}".format(self.max_cols))
        return self.max_cols

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def set_col_widths(self, line):
        """
        Set/update the maximum column widths observed in the buffer. This is
        the maximum character wdith in each column
        """
        # Get the character lengths of each element in the line
        line_len = chr_len(line)

        try:
            # Now compare with what we have now
            self.colwidths = np.maximum(line_len, self.colwidths)
        except TypeError:
            # Not initialised yet, so initialise to the current line
            self.colwidths = line_len
        except ValueError:
            # The ValueError will mean that one or the other is not the correct
            # length, so we will pad the smallest one with zeros and re-compare
            smallest = self.colwidths
            largest = line_len

            if len(smallest) > len(largest):
                smallest = line_len
                largest = self.colwidths

            # Pad the smallest one
            smallest = np.pad(smallest, (0, len(largest)-len(smallest)),
                              mode='constant')

            # Recalcuale, if this fails then we just error and do not attempt
            # anything
            self.colwidths = np.maximum(smallest, largest)

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def get_col_widths(self):
        """
        Set/update the maximum column widths observed in the buffer
        """
        if self.colwidths is not None:
            return self.colwidths
        self.set_col_widths()
        return self.colwidths

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def init_col_widths(self):
        """
        Initialise the column widths from scratch, this will loop through the
        whole buffer
        TODO: I am not 100% sure I need this method anymore??
        """

        colwidths = np.zeros(self.get_ncols())
        # self.logger.info(self.file.get_ncols())
        # self.logger.info(colwidths)
        for i in self.file.line_buffer:
            colwidths = np.maximum(chr_len(i), colwidths)

        self.max_colwidths = colwidths
        self.colwidths = np.minimum(colwidths,
                                    np.repeat(con.DEFAULT_MAX_COLWIDTH,
                                              self.file.max_cols))
        self.logger.info(self.max_colwidths)
        self.logger.info(self.colwidths)


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def chr_len(A):
    """
    Get the length of each element in an array, using Views, this seems to be
    the fastest - got from SO

    Parameters
    ----------
    A : :obj:`numpy.1darray`
        Must be character type

    Returns
    -------
    lengths :obj:`numpy.1darray`
        The lengths of the character strings in each element
    """
    try:
        v = A.view(np.uint32).reshape(A.size, -1)
    except Exception:
        print("*******************************************")
        print(A)
        raise
    i = np.argmin(v, 1)
    i[v[np.arange(len(v)), i] > 0] = v.shape[-1]
    return i


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def is_gzip(fn):
    """

    """
    pass
