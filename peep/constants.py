"""
constants used in peep
"""
import os

# Some defaults
DEFAULT_MAX_COLWIDTH = 20
DEFAULT_MAX_ROW_NAMES_WIDTH = 40
DEFAULT_PAD_CHARS = 10000
DEFAULT_PAD_ROWS = 10000
DEFAULT_NSCROLL = 5
DEFAULT_NUDGE = 1
ROW_COL_BORDER_LINE_WIDTH = 1
COL_GAP_LINE_WIDTH = 3
DEFAULT_BUFFER_MB = 5
DEFAULT_INIT_ROWS = 100
DEFAULT_BUFFER = DEFAULT_BUFFER_MB*1024*1024
DEFAULT_LOG_FILE = os.path.join(os.environ['HOME'], '.{0}.log'.format(
    __file__))
DEFAULT_DELIMITER = "\t"
DEFAULT_HEADER = True
DEFAULT_ROW_NAMES = True

# The height of the column pad. So while the column names are plotted on a
# signle line and can only be scrolled in an x-axis, we have the option of
# making the bad larger
COL_PAD_ROWS = 1

# The y-location of the column pad, so the columns should be the first row
COL_PAD_DISPLAY_TOP = 0

# This is the position of the row names pad, it should be on the far left of
# the terminal
ROW_PAD_DISPLAY_LEFT = 0

# This is the border between the row names and the columns/data, it should
# be set to at least 1 (to get a vertical line down it, but could be > 1
ROW_PAD_BORDER = 1

# For some reason the terminal rows is two shorted that what the terminal is
# so this offset is added. I need to test this some more as it might not
# be the same for all terminals
TERMINAL_ROW_OFFSET = 2
