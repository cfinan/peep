"""
Controls the rendering on things on the terminal
"""
from peep import constants as con
import numpy as np
import curses


# @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
class Render(object):
    """
    Controls the rendering on the terminal
    """
    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def __init__(self, screen, file_handler, logger,
                 pad_chars=con.DEFAULT_PAD_CHARS,
                 pad_rows=con.DEFAULT_PAD_ROWS):
        """
        Initialise

        Parameters
        ----------
        screen : :obj: `curses.Window`
            The window as returned by curses.wrapper
        file_handler : :obj:`peep.FileHandler`
            Will supply the content to render to the terminal
        logger : :obj:`logging`
            A logger for debugging
        pad_chars : int, optional, default: constants.DEFAULT_PAD_CHARS
            The width of the pad (x-axis)
        pad_rows : int, optional, default: con.DEFAULT_PAD_ROWS
            The height of the pad (y-axis)
        """
        self.screen = screen
        self.file_handler = file_handler
        self.logger = logger

        # These are the total width of the pad and the total height of the pad
        # remember that pads can be much larger than the total terminal screen
        self.PAD_WIDTH_CHARS = pad_chars
        self.PAD_HEIGHT_ROWS = pad_rows

        # The position in the data (element or 'column') that will be displayed
        # in the top-left of the data pad. It will be initialised here to the
        # top left of the file. However, if the file has column names and/or
        # row names then, the position of the top left of the file will be in
        # by one
        self.DATA_DISPLAY_ROW_START = int(self.file_handler.has_header)
        self.DATA_DISPLAY_COL_START = int(self.file_handler.has_row_names)

        # These will be adjusted everytime the user scrolls the data in the
        # terminal and will log the "virtual" position within the file of
        # the top left corner of the pad
        self.FILE_DISPLAY_ROW_START = 0
        self.FILE_DISPLAY_CHAR_START = 0

        # TODO: Temporary, see variables above that now hold this data
        self.PAD_ROW_POS = 0
        self.PAD_COL_POS = 0

        # #####################################################################

        # I will list all the various variables here that will control how
        # /where things are placed on the screen. These will all be properly
        # initialised during the rest of the initialisation, so they are all
        # just initialised to 0 here

        # Initialise the data pad display column widths. So this is an array
        # that is the same length as the maximum number of columns in the
        # data buffer. Each element will have the maximum number of characters
        # that are occupied by each column in the data buffer or the default
        # maximum column width (whichever is the smallest).
        self.DATA_PAD_DISPLAY_COL_WIDTHS = 0

        # There are 3 pads in the whole screen, the row name pad on the left
        # the column name pad on the top and the main data display pad.
        # These are the pad positional variables, they dictate the position
        # and size of each pad in the terminal. The TOP/LEFT variables indicate
        # the anchor point of the top/left of the pad with respect to the whole
        # screen. The CHARS variables are the width on the terminal of the part
        # of the pad that is displayed on the screen and the ROWS is the height
        # of the pad that is displayed on screen

        # the pad with the column names
        self.COL_PAD_DISPLAY_TOP = 0
        self.COL_PAD_DISPLAY_LEFT = 0
        self.COL_PAD_DISPLAY_CHARS = 0
        self.COL_PAD_DISPLAY_ROWS = 0

        # The pad with the row names
        self.ROW_PAD_DISPLAY_TOP = 0
        self.ROW_PAD_DISPLAY_LEFT = 0
        self.ROW_PAD_DISPLAY_CHARS = 0
        self.ROW_PAD_DISPLAY_ROWS = 0

        # The pad with all the data in it
        self.DATA_PAD_DISPLAY_TOP = 0
        self.DATA_PAD_DISPLAY_LEFT = 0
        self.DATA_PAD_DISPLAY_CHARS = 0
        self.DATA_PAD_DISPLAY_ROWS = 0

        # The pad text positions, these are the positions that place the text
        # neatly in columns with some padding between them, note that these are
        # the positions for all the text in the pad not just the text that is
        # in the actual terminal
        self.PAD_TEXT_CHAR_POS = []

        # This will hold the same data as self.PAD_TEXT_CHAR_POS only offset
        # To make the current display column to be the reference point i.e. 0
        self.PAD_TEXT_CHAR_POS_OFFSET = []

        # This is the element position of the last column in the data buffer
        # that is able to fit on the pad (in the data??)
        self.MAX_PAD_DISPLAY_DATA_COL = 0

        # The stop position for the left scroll
        self.LEFT_SCROLL_STOP = 0

        # The scroll position that activates a change in the pad contents
        self.NEWPAD_LEFT_LOAD_CHAR_POS = 0

        # The right scroll position stop point, the user is unable to scroll
        # past this
        self.RIGHT_SCROLL_STOP = 0

        # The position that when the user is scrolling right, will activate
        # a change in the pad contents
        self.NEWPAD_RIGHT_LOAD_CHAR_POS = 0

        # The last row that is able to be displayed in the pad
        self.MAX_PAD_DISPLAY_DATA_ROWS = 0

        # The stop for scrolling upwards
        self.UP_SCROLL_STOP = 0

        # The stop for scrolling downwards
        self.DOWN_SCROLL_STOP = 0

        # The position in the downward scrolling direction that will force a
        # new pad to be loaded
        self.NEWPAD_LOAD_ROW_POS = 0

        #######################################################################

        # Initialise the curses screen
        self.screen.keypad(True)
        curses.use_default_colors()
        curses.noecho()
        self.screen.refresh()

        # Initialise the column widths to display. These are based on the
        # character widths in the buffer and the maximum column width to
        # display
        self.initialise_display_column_widths()

        # Now create the pad layout
        self.create_layout()

        # Initialise the data display start, this is the row.column in the
        # data that will be at position 0,0 in the various pads
        # self.set_data_display_start()

        # These will track the location of the top-left corner of the pad on
        # the terminal
        # self.set_pad_display_start()

        # This is the offsets in chars applied to each column to generate
        # space between each column that is filled with vertical lines
        # con.COL_GAP_LINE_WIDTH should be at least 1, the default at present
        # is set to 3. A vertical line and a blank space either side
        self.COL_GAP_OFFSET = np.cumsum(
            np.repeat(con.COL_GAP_LINE_WIDTH, self.file_handler.max_cols)
        )

        # Now initialise the text positions, these are the char positions of
        # each column in the pad based on the data provided by
        # initialise_column_widths and the column_gap_offset
        self.initialise_text_positions()

        # 
        self.calc_display_columns()
        self.calc_display_rows()

        # Finally render the pads, this will fill and draw the column pad,
        # row pad and the data pad, and the lines between them
        self.draw_pads()
        self.draw_pad_separator_lines()

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def initialise_display_column_widths(self):
        """
        Initialise the widths of the columns that will be displayed, these will
        be different to the max widths of the columns in the input data file
        They are based on the character widths in the data buffer and the
        maximum column width to display
        """
        # Initialise the data pad display column widths. So this is an array
        # that is the same length as the maximum number of columns in the
        # data buffer. Each element will have the maximum number of characters
        # that are occupied by each column in the data buffer or the default
        # maximum column width (whichever is the smallest)
        self.DATA_PAD_DISPLAY_COL_WIDTHS = np.minimum(
            np.repeat(con.DEFAULT_MAX_COLWIDTH, self.file_handler.max_cols),
            self.file_handler.colwidths).astype('int')

        # The first column width maybe the row names, this is allowed to be
        # wider if the file has row_names, to prevent the text in the row names
        # being truncated un-necessarily I adjust it to match the allowed width
        # of the row names column
        if self.file_handler.has_row_names is True:
            # This basically says, take the smallest of the number of characters
            # in the first column of the buffer or the max allowed row names
            # TODO: store in a separate variable
            self.DATA_PAD_DISPLAY_COL_WIDTHS[0] = min(
                self.file_handler.max_row_names_width,
                con.DEFAULT_MAX_ROW_NAMES_WIDTH)

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def create_layout(self):
        """
        Create the screen layout
        """
        self.logger.info("****** PAD SIZES *****")

        # Ensure all the display coordinates are initialised
        self.initialise_display_coords()

        # Create some additional windows (pads) within the screen
        # The arguments to new pad are rows, cols. If the terminal is too small
        # then some of these may fail and I will need to catch these errors and
        # handle them
        # TOOD: Catch curses errors
        self.col_pad = curses.newpad(self.COL_PAD_DISPLAY_ROWS, self.PAD_WIDTH_CHARS)
        self.row_pad = curses.newpad(self.PAD_HEIGHT_ROWS, self.ROW_PAD_DISPLAY_CHARS)
        self.data_pad = curses.newpad(self.PAD_HEIGHT_ROWS, self.PAD_WIDTH_CHARS)

        # I am not 100% sure if this is needed
        self.col_pad.scrollok(True)
        self.row_pad.scrollok(True)
        self.data_pad.scrollok(True)
        self.screen.refresh()

        self.logger.info("row_pad_rows={0}".format(self.PAD_HEIGHT_ROWS))
        self.logger.info("row_pad_chars={0}".format(self.ROW_PAD_DISPLAY_CHARS))

        self.logger.info("col_pad_rows={0}".format(self.COL_PAD_DISPLAY_ROWS))
        self.logger.info("col_pad_chars={0}".format(self.PAD_WIDTH_CHARS))

        self.logger.info("data_pad_rows={0}".format(self.PAD_HEIGHT_ROWS))
        self.logger.info("data_pad_chars={0}".format(self.PAD_WIDTH_CHARS))

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def initialise_display_coords(self):
        """
        Initialise all the cordinates needed to work on the terminal. This sets
        the coordinates of each of the pads relative to the screen coordinates
        """
        # Get the terminal dimentions
        # TODO: Adjust all of these for terminal size, i.e. pad the required
        # TODO: pads actually fit on the terminal? And also, needs to be called
        # TODO: to adapt for a terminal resize events
        rows, chars = self.screen.getmaxyx()

        # The terminal dimentions are total width and total height. The
        # coordinates for plotting are 0 based so we subtract 1 to get the
        # plottable width and height
        rows -= 1
        chars -= 1

        # Determine what the row names width will be as this can be different
        # from the data col names width
        row_names_width = min(self.file_handler.max_row_names_width,
                              con.DEFAULT_MAX_ROW_NAMES_WIDTH)

        # The row position that the column pad is displayed from, thisi will
        # be 0 in pretty much all cases
        self.COL_PAD_DISPLAY_TOP = con.COL_PAD_DISPLAY_TOP

        # This is where the left hand edge of the column pad starts. It will be
        # to the right of the end of the row pad + whatever border we have set
        # on the row pad
        self.COL_PAD_DISPLAY_LEFT = row_names_width + con.ROW_PAD_BORDER

        # The width of the column pad display. Note this is not the total width
        # of the pad but rather, the part of the pad that is displayed on the
        # terminal. The same applies to the col pad display rows
        self.COL_PAD_DISPLAY_CHARS = chars
        self.COL_PAD_DISPLAY_ROWS = con.COL_PAD_ROWS

        # This is the top position of the row pad with respect to the top
        # of the screen, it should start below the column pad + what ever
        # gap there is between the column pad and the data pad 
        self.ROW_PAD_DISPLAY_TOP = con.COL_PAD_ROWS + \
            con.ROW_COL_BORDER_LINE_WIDTH
        self.ROW_PAD_DISPLAY_LEFT = con.ROW_PAD_DISPLAY_LEFT

        # The row pad is wide enough to fit the specified number of
        # row_names in it
        self.ROW_PAD_DISPLAY_CHARS = row_names_width
        self.ROW_PAD_DISPLAY_ROWS = rows - self.ROW_PAD_DISPLAY_TOP + \
            con.TERMINAL_ROW_OFFSET

        self.DATA_PAD_DISPLAY_TOP = self.ROW_PAD_DISPLAY_TOP
        self.DATA_PAD_DISPLAY_LEFT = self.COL_PAD_DISPLAY_LEFT
        self.DATA_PAD_DISPLAY_CHARS = self.COL_PAD_DISPLAY_CHARS
        self.DATA_PAD_DISPLAY_ROWS = self.ROW_PAD_DISPLAY_ROWS

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def initialise_text_positions(self):
        """
        Work out where all the text will be placed on screen. This deals with
        with the character positions on the pad not the row positions as they
        are pretty easy to deal with
        """
        # The pad text positions, these are the positions that place the text
        # neatly in columns with some padding between them, note that these are
        # the positions for all the text in the pad not just the text that is
        # in the actual terminal
        self.PAD_TEXT_CHAR_POS = np.cumsum(
            self.DATA_PAD_DISPLAY_COL_WIDTHS).astype('int') + \
            self.COL_GAP_OFFSET

        # At this point the text is offset a bit too much, as we need the first
        # display column to be at 0+the padding between the row names and data
        # So this state ment either adjusts the first element to 0 (+pad) - in
        # the case of the data having no row names, or the second element to 0
        # (+pad) in the case of the data having row names
        self.PAD_TEXT_CHAR_POS = self.PAD_TEXT_CHAR_POS -  \
            self.PAD_TEXT_CHAR_POS[int(self.file_handler.has_row_names)] + \
            con.ROW_PAD_BORDER

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def calc_display_columns(self):
        """
        This calculates how many display columns will fit in the pad_chars.
        A display column - equates to a data array element. How many elements
        that can be displayed in the pad is based on the column widths + any
        gaps between the columns. This also calculates the refresh position.
        The point in the scroll right where a new pad will be needed, or rather
        when new data will be needed to be loaded into the existing pad
        """
        # rows, chars = self.screen.getmaxyx()
        
        # Need an offset into the array to allow for scrolling through
        # the columns, i.e. column 0 is not always at the left of the pad.
        # So this subtracts the coordinates for the first displayed column in
        # the pad away from all of the columns, to make the first column
        # displayed in the pad to 0. Upon first initialsation this will have
        # no effect but after paging through the file it might not be
        self.PAD_TEXT_CHAR_POS_OFFSET = self.PAD_TEXT_CHAR_POS - \
            self.PAD_TEXT_CHAR_POS[self.DATA_DISPLAY_COL_START] + 1
        # self.logger.info(self.PAD_TEXT_CHAR_POS)
        # self.logger.info(PAD_DISPLAY_DATA_COL_OFFSET)
        # self.logger.info(len(self.PAD_TEXT_CHAR_POS))
        # self.logger.info(PAD_DISPLAY_DATA_COL_OFFSET)

        # Now we want to determine the max column (array element) that can
        # fit in the pad. searchsorted (ss) will return the element number
        # where the search would sit, if it was placed in the array (when
        # "left" is used). So, the max display column that will fill in
        # the pad is two before this (ss-2). ss-1 may fit in the pad if the
        # difference between ss-1 + (ss - ss-1) < pad width. However, if ss
        # is 0 it will need to stay at 0 and if it is
        # len(self.PAD_TEXT_CHAR_POS) then as long as
        # self.PAD_TEXT_CHAR_POS[-1] + self.DATA_PAD_DISPLAY_COL_WIDTHS[-1]
        # <= PAD_WIDTH_CHARS it can stay at ss
        self.MAX_PAD_DISPLAY_DATA_COL = np.searchsorted(
            self.PAD_TEXT_CHAR_POS_OFFSET, self.PAD_WIDTH_CHARS, side='left')
        self.logger.info(self.MAX_PAD_DISPLAY_DATA_COL)
        # Now, if we are not the last column, i.e. the data is smaller than the
        # pad
        if self.MAX_PAD_DISPLAY_DATA_COL < len(self.PAD_TEXT_CHAR_POS):
            # Make sure we do not go below 0
            self.MAX_PAD_DISPLAY_DATA_COL = \
                max(0, self.MAX_PAD_DISPLAY_DATA_COL - 2)
        else:
            # We think we can fit the whole thing in we test if that is true
            # by summing the last character position and the last text
            # length if they are > than what we can fit in the pad then we
            # move to the one before
            self.MAX_PAD_DISPLAY_DATA_COL -= \
            (self.PAD_TEXT_CHAR_POS[-1] + self.DATA_PAD_DISPLAY_COL_WIDTHS[-1])\
             > self.PAD_WIDTH_CHARS

        self.MAX_PAD_DISPLAY_DATA_COL -= \
            (self.MAX_PAD_DISPLAY_DATA_COL == len(self.PAD_TEXT_CHAR_POS_OFFSET))
            
        self.logger.info(self.MAX_PAD_DISPLAY_DATA_COL)
        # TODO: Do I need to take into accoun the cursor postion - I do not
        # TODO: think so as we are working from the top left
        self.LEFT_SCROLL_STOP = 0

        # TODO: will need redefining when paging is enabled
        self.NEWPAD_LEFT_LOAD_CHAR_POS = self.LEFT_SCROLL_STOP

        # The right scroll stop position is the pad coordinates of the last
        # displayed column + the length of the last displayed column + the gap
        # after the last displayed column - the width of the part of the pad
        # that is being displayed in the terminal (to offset to the top left)
        self.RIGHT_SCROLL_STOP = \
            self.PAD_TEXT_CHAR_POS_OFFSET[self.MAX_PAD_DISPLAY_DATA_COL] \
            + self.DATA_PAD_DISPLAY_COL_WIDTHS[self.MAX_PAD_DISPLAY_DATA_COL] + \
            con.COL_GAP_LINE_WIDTH - self.DATA_PAD_DISPLAY_CHARS

        self.NEWPAD_RIGHT_LOAD_CHAR_POS = self.RIGHT_SCROLL_STOP

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def calc_display_rows(self):
        """
        This calculates how many display rows will fit in the data pad and row
        pad rows. This also calculates the refresh position. The point in the
        scroll right where a new pad will be needed, or rather, when new data
        will need to be loaded into the existing pad
        """
        # The maximum display column is the current position + either the total
        # pad height-1 or the length of the current buffer. Whichever is
        # smaller
        self.MAX_PAD_DISPLAY_DATA_ROWS = self.DATA_DISPLAY_ROW_START + \
            min(self.PAD_HEIGHT_ROWS - 1,
                len(self.file_handler.line_buffer) - 1)

        # The stop point for scrolling upwards
        self.UP_SCROLL_STOP = 0

        # At the moment this is setup so that if the data buffer does not fit
        # on the pad then this will scroll beyond it into the ether
        self.DOWN_SCROLL_STOP = len(self.file_handler.line_buffer) - \
            self.DATA_PAD_DISPLAY_ROWS
        self.NEWPAD_LOAD_ROW_POS = len(self.file_handler.line_buffer) - \
            self.DATA_PAD_DISPLAY_ROWS

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def draw_pads(self):
        """
        Draw the column, row and data pad with values representing the current
        position
        """

        # First we render the data pad, what we will display on the data
        # pad will change depending of if we have a header/rownames, the size
        # of the pad and the size of the data buffer both in terms of rows and
        # columns

        # We loop through the current data rows that we want to render onto the
        # pad and the current data columns that we want to render onto the pad
        for row_pad_pos, row in enumerate(range(self.DATA_DISPLAY_ROW_START,
                                                self.MAX_PAD_DISPLAY_DATA_ROWS)):
            for col in range(self.DATA_DISPLAY_COL_START,
                             self.MAX_PAD_DISPLAY_DATA_COL):
                # Draw the data on the data pad. The arguments are row
                # position in the pad, column position in the pad and the actual
                # string being output
                self.data_pad.addstr(
                    row_pad_pos,
                    self.PAD_TEXT_CHAR_POS_OFFSET[col],
                    self.format_str(self.file_handler.line_buffer[row][col],
                                    self.DATA_PAD_DISPLAY_COL_WIDTHS[col]))

        # get the current terminal size
        rows, chars = self.screen.getmaxyx()

        # Here we want to add the column labels and vertical line dividers
        # between the columns in both the header and the data pad
        for col in range(self.DATA_DISPLAY_COL_START,
                         self.MAX_PAD_DISPLAY_DATA_COL):
            self.col_pad.addstr(self.COL_PAD_DISPLAY_TOP,
                                self.PAD_TEXT_CHAR_POS_OFFSET[col],
                                self.format_str(self.file_handler.header[col],
                                                self.DATA_PAD_DISPLAY_COL_WIDTHS[col]),
                                curses.A_BOLD)

            self.data_pad.vline(self.DATA_PAD_DISPLAY_TOP,
                                max(0, self.PAD_TEXT_CHAR_POS_OFFSET[col] - 2),
                                curses.ACS_VLINE,
                                self.PAD_HEIGHT_ROWS)
            self.col_pad.vline(self.COL_PAD_DISPLAY_TOP,
                               max(0, self.PAD_TEXT_CHAR_POS_OFFSET[col] - 2),
                               curses.ACS_VLINE,
                               self.PAD_HEIGHT_ROWS)
        
        # for col in range(self.DATA_DISPLAY_COL_START,
        #                  self.MAX_PAD_DISPLAY_DATA_COL):
        #     self.data_pad.vline(0, self.PAD_TEXT_CHAR_POS[col+1]-2, curses.ACS_VLINE,
        #                         len(self.file_handler.line_buffer))
        #     self.col_pad.vline(0, self.PAD_TEXT_CHAR_POS[col+1]-2, curses.ACS_VLINE, 2)
        #     self.col_pad.addstr(0, self.PAD_TEXT_CHAR_POS[col],
        #                         self.format_str(self.file_handler.header[col],
        #                                         self.DATA_PAD_DISPLAY_COL_WIDTHS[col]),
        #                         curses.A_BOLD)

        self.screen.vline(0, self.ROW_PAD_DISPLAY_CHARS, curses.ACS_VLINE, rows)
        for row in range(int(self.file_handler.has_header), len(self.file_handler.line_buffer)):
            # self.logger.info("header={0}".format(self.file_handler.header[ch]))
            self.row_pad.addstr(row - 1, 0,
                                self.format_str(self.file_handler.row_names[row],
                                                self.DATA_PAD_DISPLAY_COL_WIDTHS[0]),
                                curses.A_BOLD)

        # Now refresh the various pads
        self.refresh_data_pad()
        self.refresh_col_pad()
        self.refresh_row_pad()

        self.log_coords()

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def refresh_data_pad(self):
        """
        Refresh the data pad
        """
        self.data_pad.refresh(self.PAD_ROW_POS, self.PAD_COL_POS,
                              self.DATA_PAD_DISPLAY_TOP,
                              self.DATA_PAD_DISPLAY_LEFT,
                              self.ROW_PAD_DISPLAY_ROWS,
                              self.COL_PAD_DISPLAY_CHARS)

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def refresh_col_pad(self):
        """
        Refresh the data pad
        """
        self.col_pad.refresh(0, self.PAD_COL_POS,
                             self.COL_PAD_DISPLAY_TOP,
                             self.COL_PAD_DISPLAY_LEFT,
                             self.COL_PAD_DISPLAY_ROWS,
                             self.COL_PAD_DISPLAY_CHARS)

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def refresh_row_pad(self):
        """
        Refresh the data pad
        """
        self.row_pad.refresh(self.PAD_ROW_POS, 0,
                             self.ROW_PAD_DISPLAY_TOP,
                             self.ROW_PAD_DISPLAY_LEFT,
                             self.ROW_PAD_DISPLAY_ROWS,
                             self.ROW_PAD_DISPLAY_CHARS)

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def draw_pad_separator_lines(self):
        """
        This draws the dividing lines between the column pad/data pad and the
        row pad/data pad. These dividing lines are drawn at screen level not
        pad level, as they are static
        """
        # get the current terminal size
        rows, chars = self.screen.getmaxyx()

        # The arguments are y, x, char, length (either nrows of nchars)
        self.screen.vline(0, self.ROW_PAD_DISPLAY_CHARS, curses.ACS_VLINE, rows)
        self.screen.hline(self.COL_PAD_DISPLAY_ROWS, 0, curses.ACS_HLINE, chars)

        # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    # def draw_pads(self):
    #     """
    #     Draw the column, row and data pad with values representing the current
    #     position
    #     """

    #     # First we render the data pad, what we will display on the data
    #     # pad will change depending of if we have a header/rownames, the size
    #     # of the pad and the size of the data buffer both in terms of rows and
    #     # columns

    #     # So when paging through a file, there will be a difference between
    #     # the pad coordinates which will be in the range:
    #     # 0 -> (height/width - 1) and the row/column positions in the datafile
    #     # Unless we are at the top left of the file then we will need to apply
    #     # an offset. based on the current top left position in file character
    #     # coordinates and 0,0 pad top left position
    #     # 
    #     for row in range(int(self.file_handler.has_header), len(self.file_handler.line_buffer)):
    #         for ch in range(int(self.file_handler.has_row_names), self.MAX_PAD_DISPLAY_DATA_COL):
    #             try:
    #                 self.data_pad.addstr(
    #                     row - 1,
    #                     self.PAD_TEXT_CHAR_POS[ch],
    #                     self.format_str(self.file_handler.line_buffer[row][ch],
    #                                     self.DATA_PAD_DISPLAY_COL_WIDTHS[ch]))
    #                 # self.pad.addch(y+1, self.pady_text[x] - 2, curses.ACS_VLINE)
    #             except IndexError:
    #                 # Overshoot
    #                 break

    #     rows, chars = self.screen.getmaxyx()

    #     # y, x, char, nrows
    #     self.screen.hline(1, 0, curses.ACS_HLINE, chars)
    #     self.screen.vline(0, self.ROW_PAD_DISPLAY_CHARS, curses.ACS_VLINE, rows)

    #     for ch in range(int(self.file_handler.has_row_names), self.MAX_PAD_DISPLAY_DATA_COL):
    #         try:
    #             self.data_pad.vline(0, self.PAD_TEXT_CHAR_POS[ch+1]-2, curses.ACS_VLINE,
    #                                 len(self.file_handler.line_buffer))
    #             self.col_pad.vline(0, self.PAD_TEXT_CHAR_POS[ch+1]-2, curses.ACS_VLINE, 2)
    #         except IndexError:
    #             pass
    #         self.col_pad.addstr(0, self.PAD_TEXT_CHAR_POS[ch],
    #                             self.format_str(self.file_handler.header[ch],
    #                                             self.DATA_PAD_DISPLAY_COL_WIDTHS[ch]),
    #                             curses.A_BOLD)

    #     self.screen.vline(0, self.ROW_PAD_DISPLAY_CHARS, curses.ACS_VLINE, rows)
    #     for row in range(int(self.file_handler.has_header), len(self.file_handler.line_buffer)):
    #         # self.logger.info("header={0}".format(self.file_handler.header[ch]))
    #         self.row_pad.addstr(row - 1, 0,
    #                             self.format_str(self.file_handler.row_names[row],
    #                                             self.DATA_PAD_DISPLAY_COL_WIDTHS[0]),
    #                             curses.A_BOLD)

    #     self.data_pad.refresh(self.PAD_ROW_POS, self.PAD_COL_POS,
    #                           self.DATA_PAD_DISPLAY_TOP,
    #                           self.DATA_PAD_DISPLAY_LEFT,
    #                           self.ROW_PAD_DISPLAY_ROWS,
    #                           self.COL_PAD_DISPLAY_CHARS)

    #     self.col_pad.refresh(0, self.PAD_COL_POS,
    #                          self.COL_PAD_DISPLAY_TOP,
    #                          self.COL_PAD_DISPLAY_LEFT,
    #                          self.COL_PAD_DISPLAY_ROWS,
    #                          self.COL_PAD_DISPLAY_CHARS)

    #     self.row_pad.refresh(self.PAD_ROW_POS, 0,
    #                          self.ROW_PAD_DISPLAY_TOP,
    #                          self.ROW_PAD_DISPLAY_LEFT,
    #                          self.ROW_PAD_DISPLAY_ROWS,
    #                          self.ROW_PAD_DISPLAY_CHARS)

    #     self.log_coords()

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def log_coords(self):
        self.logger.info("****** CURRENT COORDS ******")
        # Get the terminal dimentions
        rows, chars = self.screen.getmaxyx()
        self.logger.info("terminal_rows={0}".format(rows))
        self.logger.info("terminal_chars={0}".format(chars))
        for i in ['PAD_ROW_POS', 'PAD_COL_POS', 'DATA_PAD_DISPLAY_LEFT',
                  'DATA_PAD_DISPLAY_TOP', 'COL_PAD_DISPLAY_CHARS',
                  'ROW_PAD_DISPLAY_ROWS']:
            self.logger.info('{0}={1}'.format(i, getattr(self, i)))

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def format_str(self, text, text_len):
        """
        Fit the string to the column, not the most efficient way of doing it
        but works for now
        TODO: Investigate views doe this
        """
        if len(text) > text_len:
            return text[:int(text_len)-3]+'...'
        return text

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def scroll_right(self, nscroll=1):
        """
        Called on a scroll right event
        """
        col_scroll = min(self.RIGHT_SCROLL_STOP, self.PAD_COL_POS + nscroll)
        self.scroll_cols(col_scroll)

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def scroll_left(self, nscroll=1):
        """
        Scroll the pad left, called in response to a left arrow key
        """
        col_scroll = max(self.LEFT_SCROLL_STOP, self.PAD_COL_POS - nscroll)
        self.scroll_cols(col_scroll)

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def scroll_cols(self, col_scroll):
        """
        Scroll the columns
        """
        self.data_pad.refresh(self.PAD_ROW_POS, col_scroll,
                              self.DATA_PAD_DISPLAY_TOP,
                              self.DATA_PAD_DISPLAY_LEFT,
                              self.ROW_PAD_DISPLAY_ROWS,
                              self.COL_PAD_DISPLAY_CHARS)

        # refresh the column pad
        self.col_pad.refresh(self.COL_PAD_DISPLAY_TOP, col_scroll,
                             self.COL_PAD_DISPLAY_TOP,
                             self.COL_PAD_DISPLAY_LEFT,
                             self.COL_PAD_DISPLAY_ROWS,
                             self.COL_PAD_DISPLAY_CHARS)
        self.PAD_COL_POS = col_scroll

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def scroll_up(self, nscroll=1):
        """
        Called on a scroll up event
        """
        row_scroll = max(self.UP_SCROLL_STOP, self.PAD_ROW_POS - nscroll)
        self.scroll_rows(row_scroll)

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def scroll_down(self, nscroll=1):
        """
        Called on a scroll down event
        """
        row_scroll = min(self.DOWN_SCROLL_STOP, self.PAD_ROW_POS + nscroll)
        self.scroll_rows(row_scroll)

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def scroll_rows(self, row_scroll):
        """
        Scroll the rows by row_scroll amount
        """
        self.data_pad.refresh(row_scroll, self.PAD_COL_POS,
                              self.DATA_PAD_DISPLAY_TOP,
                              self.DATA_PAD_DISPLAY_LEFT,
                              self.ROW_PAD_DISPLAY_ROWS,
                              self.COL_PAD_DISPLAY_CHARS)

        # refresh the row pad
        self.row_pad.refresh(row_scroll, self.ROW_PAD_DISPLAY_LEFT,
                             self.ROW_PAD_DISPLAY_TOP,
                             self.ROW_PAD_DISPLAY_LEFT,
                             self.ROW_PAD_DISPLAY_ROWS,
                             self.ROW_PAD_DISPLAY_CHARS)

        self.PAD_ROW_POS = row_scroll
