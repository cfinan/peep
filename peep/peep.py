"""
Pretty viewing of files on the command line
"""
from peep import readers, render, constants as con
import argparse
import curses
import sys
import logging
import os

# Declare in main, all these will be updated below
DEFAULT_MAX_COLWIDTH = 0
DEFAULT_PAD_CHARS = 0
DEFAULT_PAD_ROWS = 0
DEFAULT_NSCROLL = 0
DEFAULT_NUDGE = 0
DEFAULT_BUFFER_MB = 0
DEFAULT_LOG_FILE = ""
DEFAULT_DELIMITER = ""

# Now we want to see if the user has defined any enviroment variables to
# globally overide the defaults
DEFAULTS = [('DEFAULT_MAX_COLWIDTH', int), ('DEFAULT_PAD_ROWS', int),
            ('DEFAULT_PAD_CHARS', int), ('DEFAULT_NSCROLL', int),
            ('DEFAULT_NUDGE', int), ('DEFAULT_LOG_FILE', str),
            ('DEFAULT_BUFFER_MB', int), ('DEFAULT_DELIMITER', str)]
thismodule = sys.modules[__name__]

# Loop through the defaults and search for an enviroment variable
for arg, cast in DEFAULTS:
    try:
        # A KeyError will mean it is not set
        env_arg = 'PEEP_{0}'.format(arg)
        setattr(thismodule, arg, cast(os.environ[env_arg]))
    except KeyError:
        # It has not been set as an enviromant variable so we set from
        # constants
        setattr(thismodule, arg, getattr(con, arg))
    except TypeError:
        # A type error will mean the cast has not worked so the variable
        # may be set incorrectly
        raise TypeError("can't cast '{0}': are you sure it is set "
                        "correctly?".format(env_arg))


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def main():
    """
    Main entry point for peep
    """
    curses.wrapper(run_peep)


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def run_peep(screen):
    """
    This is the initialisation function from ncurses

    Parameters
    ----------
    screen
        Passed by curses.wrapper
    """
    # sort out the command line arguments
    parser = init_cmd_args()
    args = parse_cmd_args(parser)

    # Get the terminal size, we can adjust the pad_chars, pad_rows to the
    # terminal size
    rows, chars = screen.getmaxyx()
    args.pad_rows = max(args.pad_rows, rows)
    args.pad_chars = max(args.pad_chars, chars)

    infile = args.infile
    if infile is None:
        infile = sys.stdin

    # As I can't echo anything out, I iwll use the logger to output
    logger = init_logger(args.log, level=args.log_level)
    logger.info("starting initialisation...")

    # Initialise the file handler
    with readers.CsvFileHandler(infile,
                                mem_buffer=args.buffer*1024*1024,
                                init_rows=rows,
                                logger=logger,
                                delimiter=args.delimiter) as file_handler:
        # Finally lauch the peep application
        peep_launch(
            screen,
            file_handler,
            logger,
            pad_chars=args.pad_chars,
            pad_rows=args.pad_rows
        )


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def init_cmd_args():
    """
    Initialise the main command line arguments (but do not parse them)

    Returns
    -------
    parser : :obj:`argparse.ArgumentParser`
        The parser object
    """
    parser = argparse.ArgumentParser(
        description='A viewer for tabular data in the terminal')
    parser.add_argument('infile', nargs='?',
                        help='The input file, if omitted then it is assumed '
                        'that input is from STDIN')
    parser.add_argument('-b', '--buffer', type=int, default=DEFAULT_BUFFER_MB,
                        help='The memory allocated to the data buffer '
                        '(default={0}'.format(DEFAULT_BUFFER_MB))
    parser.add_argument('-c', '--pad-chars', type=int, default=DEFAULT_PAD_CHARS,
                        help='The number of characters to display in the pad '
                        'in the x direction (default={0})'.
                        format(DEFAULT_PAD_CHARS))
    parser.add_argument('-r', '--pad-rows', type=int, default=DEFAULT_PAD_ROWS,
                        help='The number of rows to display in the pad '
                        'in the y direction (default={0})'.
                        format(DEFAULT_PAD_ROWS))
    parser.add_argument('-d', '--delimiter', type=str, default=DEFAULT_DELIMITER,
                        help='The default delimiter (default={0})'.
                        format(DEFAULT_DELIMITER))
    parser.add_argument('-l', '--log', type=str,
                        default=os.path.join(os.environ['HOME'], ".peep.log"),
                        help='Path to a log file, when defined a log file is'
                        ' generated')
    return parser


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def parse_cmd_args(parser):
    """
    Parse the command line arguments

    Parameters
    ----------
    parser : :obj:`argparse.ArgumentParser`
        The parser object

    Returns
    -------
    args : :obj:`Namespace`
        The arguments after calling `parser.parse_args`
    """
    args = parser.parse_args()

    # set up the logging
    args.log_level = logging.NOTSET
    if args.log is not None:
        args.log_level = logging.DEBUG

    # Make sure the buffer is a positive value
    if args.buffer <= 0:
        raise ValueError("--buffer must be a positive value")

    return args


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def init_logger(logfile, level=logging.NOTSET):
    """
    Initialise the logger, this is mainly used for debugging

    Parameters
    ----------
    logfile :str
        The name of the log file
    level :int (optional)
        The logging level the (default=0) (not set)
    """
    logger = logging.getLogger(__file__)
    handler = logging.FileHandler(logfile, mode='w')
    formatter = logging.Formatter('%(asctime)s %(levelname)s %(message)s')
    handler.setFormatter(formatter)
    logger.addHandler(handler)
    logger.setLevel(level)
    logger.info("******* LOG START ********")

    return logger


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def peep_launch(screen, file_handler, logger, pad_chars=DEFAULT_PAD_CHARS,
                pad_rows=DEFAULT_PAD_ROWS, nscroll=DEFAULT_NSCROLL,
                nudge=DEFAULT_NUDGE):
    """
    Now actually start to draw stuff to screen and await user interaction

    Parameters
    ----------
    screen : :obj:`curses.Window`
        The stdscr object created by the curses.wrapper call
    file_handler : :obj:`readers.CsvFileHandler`
        The file handler for interacting with the input
    logger : :obj:`logging`
        The logging object to use when debugging
    pad_chars : int, optional, default: DEFAULT_PAD_CHARS (environment variable)
        The width of the pad in number of characters
    pad_rows : int, optional, default: DEFAULT_PAD_ROWS (environment variable)
        The height of the pad in rows
    nscroll : int, optional, default: DEFAULT_NSCROLL (environment variable)
        The number of rows or chrs to scroll
    nudge : int, optional, default: DEFAULT_NUDGE (environment variable)
        The number of rows or chrs to nudge
    """
    # Log some stuff
    logger.info(file_handler)
    logger.info("UpKey={0}".format(curses.KEY_UP))
    logger.info("DownKey={0}".format(curses.KEY_DOWN))
    logger.info("LeftKey={0}".format(curses.KEY_LEFT))
    logger.info("RightKey={0}".format(curses.KEY_RIGHT))

    # Initialise the rendering, this sets up the screen
    ren = render.Render(
        screen,
        file_handler,
        logger,
        pad_chars=pad_chars,
        pad_rows=pad_rows
    )

    # Wait for user to scroll or quit
    running = True
    while running:
        # Get an event in the terminal
        ch = screen.getch()

        try:
            # Attempt to convert into a character
            strchr = chr(ch)
        except ValueError:
            strchr = ''

        if strchr == 'q':
            # Do we want to quit
            running = False
        elif strchr == 'a':
            # I can't capture alt events so I have used a as a proxy for now
            # if that is pressed then we look for a second key press on the
            # scroll keys, this will indicate a nudge
            ch2 = screen.getch()
            check_scroll(ch2, ren, nudge)
        else:
            # Finally just check the scroll
            check_scroll(ch, ren, nscroll)
        # elif ch == curses.KEY_RESIZE:
        #     height,width = scr.getmaxyx()
        #     while mypad_pos > mypad.getyx()[0] - height - 1:
        #       mypad_pos -= 1
        #     mypad_refresh()


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def check_scroll(ch, ren, nscroll):
    """
    This deals with the scroll events. I have farmed this out into a separate
    function as I wanted to implement a 1 character scroll when CTRL is pressed

    Parameters
    ----------
    ch : int
        The character press captured from getch
    ren : :obj:`render.Render`
        The render class that will do the scrolling
    nscroll : int
        The number of rows or chrs to scroll
    """
    if ch == curses.KEY_DOWN:
        ren.scroll_down(nscroll=nscroll)
        # elif ch == curses.KEY_UP and mypad_pos > -2:
    elif ch == curses.KEY_UP:
        ren.scroll_up(nscroll=nscroll)
    elif ch == curses.KEY_RIGHT:
        ren.scroll_right(nscroll=nscroll)
    elif ch == curses.KEY_LEFT:
        ren.scroll_left(nscroll=nscroll)


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
if __name__ == '__main__':
    main()
