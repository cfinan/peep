=======
Scripts
=======

There is only a single script that is installed and that is peep2. It will eventually be renamed to peep when it is complete.

``peep2``
---------

.. argparse::
   :module: peep.peep
   :func: _init_cmd_args
   :prog: peep2
