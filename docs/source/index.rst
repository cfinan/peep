.. peep documentation master file.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to peep
===============

`peep <https://gitlab.com/cfinan/peep>`_ is a tabular viewer for the terminal. This version is a prototype version that will display the first few rows in a file. There are a few issues with it, these are outlined in the package README file.

I do have the original version that I wrote sometime ago which pipes a formatted table into ``less``, Please see the `pyaddons <https://gitlab.com/cfinan/pyaddons>`_ repository for details.

Contents
========

.. toctree::
   :maxdepth: 2
   :caption: Setup

   getting_started

.. toctree::
   :maxdepth: 2
   :caption: Programmer reference

   scripts

.. toctree::
   :maxdepth: 2
   :caption: Project admin

   contributing

Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
