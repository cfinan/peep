# peep

__version__: `0.1.1a0`

peep is a tabular viewer for the terminal. This version is a prototype version that will display the first few rows in a file. There are a few issues with it, these are outlined below.

I do have the original version that I wrote sometime ago which pipes a formatted table into `less`, Please see the [pyaddons](https://gitlab.com/cfinan/pyaddons) repository for details.

There is [online](https://cfinan.gitlab.io/peep/index.html) documentation for peep.

## Issues
There are a few issues with this prototype version:

1. It will only display the first few rows in a file.
2. There are some minor issues with the grid formatting.
3. If the file does not fill the whole terminal, it will throw an error.

For latest updates to peep, please see the `dev` branch. I will endeavour to keep the dev branch following into master as quickly as possible and update the various issues here.

## Installation instructions
At present, peep is undergoing development and no packages exist yet on PyPi. Therefore it is recommended that it is installed in either of the two ways listed below.

### Installation using pip
You can install using pip from the root of the cloned repository, first clone and cd into the repository root:

```
git clone git@gitlab.com:cfinan/peep.git
cd peep
```

Install the dependencies:
```
python -m pip install --upgrade -r requirements.txt
```

Then install using pip
```
python -m pip install .
```

Or for an editable (developer) install run the command below from the root of the peep repository. The difference with this is that you can just to a `git pull` to update multi_join, or switch branches without re-installing:
```
python -m pip install -e .
```

### Installation using conda
I maintain a conda package in my personal conda channel. To install this please run:

```
conda install -c cfin -c conda-forge peep
```

### Conda dependencies
There are also conda yaml environment files in `./resources/conda/env` that have the same contents as `requirements.txt` but for conda packages, so all the pre-requisites. I use this to install all the requirements via conda and then install the package as an editable pip install.

However, if you find these useful then please use them. There are Conda environments for Python v3.8, v3.9 and v3.10.

## Run tests
If you have cloned the repository, you can also run the tests using `pytest ./tests`, if any fail please contact us (see the contribution page for contact info).
