#!/usr/bin/env python
import curses
import string
import logging
import numpy as np


def add_columns(screen, pad):
    """
    Add columns
    """
    y = 0
    x = 0
    chars = string.ascii_lowercase + string.ascii_uppercase
    chars = chars + chars
    pos = np.arange(len(chars))
    pos = pos + np.cumsum(np.repeat(3, len(chars)))
    for i in range(len(chars)):
        try:
            pad.addstr(0, pos[i]-3, chars[i])
        except Exception:
            pass
    height, width = screen.getmaxyx()
    pad.refresh(0, 0, 0, 6, 1, width-1)

def add_rows(screen, pad):
    """
    Add columns
    """
    for i in range(100):
        pad.addstr(i, 0, str(i))
    height, width = screen.getmaxyx()
    pad.refresh(0, 0, 1, 0, height-1, 5)
        

def add_data():
    """
    Add some data
    """
    pass

# def create_layout(screen):
#     """
#     Create the screen layout
#     """
#     height, width = screen.getmaxyx()

#     # Create some additional windows within the screen
#     # curses.newwin(nlines, ncols, begin_y, begin_x)
#     cols = curses.newpad(1, width+100)
#     cols.scrollok(True)
#     rows = curses.newwin(height-1, 4, 1, 0)
#     data = curses.newwin(height-1, width-4, 1, 4)

#     # Draw boxes arounf the windows
#     cols.box()
#     rows.box()
#     data.box()

#     # Refresh to display the boxed windows
#     screen.refresh()
#     cols.refresh(0, 0, 0, 0, 0, 0)
#     rows.refresh()
#     data.refresh()
#     return rows, cols, data


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def create_layout(screen):
    """
    Create the screen layout
    """
    # Get the terminal dimentions 
    height, width = screen.getmaxyx()

    # Create some additional windows within the screen
    cols = curses.newpad(1, width+100)
    rows = curses.newpad(height+100, 5)
    cols.scrollok(True)
    rows.scrollok(True)
    # rows = curses.newwin(height-1, 4, 1, 0)
    # data = curses.newwin(height-1, width-4, 1, 4)

    # Draw boxes arounf the windows
    # cols.box()
    rows.box()
    # data.box()

    # Refresh to display the boxed windows
    screen.refresh()
    cols.refresh(0, 0, 0, 6, 1, width-1)
    rows.refresh(0, 0, 1, 0, height-1, 5)
    # rows.refresh()
    # data.refresh()
    # return rows, cols, data
    return rows, cols, None


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def create_pads(rows, cols, data):
    """
    Create oversized pads within the windows
    """
    # row_pad = rows.subpad(500, 4, 1, 0)
    row_pad = None
    col_pad = cols.subpad(1, 220, 0, 0)
    # data_pad = data.subpad(500, 200, 2, 5)
    data_pad = None
    return row_pad, col_pad, data_pad


def scroll_up(logger, screen, rows, xpos, ypos):
    nrows, ncolumns = screen.getmaxyx()
    logger.info("NROWS={0}".format(nrows))
    logger.info("NCOLS={0}".format(ncolumns))
    logger.info("YPOS={0}".format(ypos))
    # cols.refresh(0,
    #              xpos+5,
    #              0,
    #              0,
    #              1, columns-1)
    # Start printing text from (0,xpos) of the pad (first line, 3rd char)
    # on the screen at position (5,5)
    # with the maximum portion of the pad displayed being 20 chars x 15 lines
    # Since we only have one line, the 15 lines is overkill, but the 20 chars
    # will only show 20 characters before cutting off
    rows.refresh(ypos-5,
                 0,1, 0, nrows-1, 5)
    # screen.refresh()
    # cols.refresh()
    return ypos-5


def scroll_down(logger, screen, rows, xpos, ypos):
    nrows, ncolumns = screen.getmaxyx()
    logger.info("NROWS={0}".format(nrows))
    logger.info("NCOLS={0}".format(ncolumns))
    logger.info("YPOS={0}".format(ypos))
    # cols.refresh(0,
    #              xpos+5,
    #              0,
    #              0,
    #              1, columns-1)
    # Start printing text from (0,xpos) of the pad (first line, 3rd char)
    # on the screen at position (5,5)
    # with the maximum portion of the pad displayed being 20 chars x 15 lines
    # Since we only have one line, the 15 lines is overkill, but the 20 chars
    # will only show 20 characters before cutting off
    rows.refresh(ypos+5,
                 0,1, 0, nrows-1, 5)
    # screen.refresh()
    # cols.refresh()
    return ypos+5

def scroll_right(logger, screen, cols, xpos, ypos):
    rows, columns = screen.getmaxyx()
    logger.info("NROWS={0}".format(rows))
    logger.info("NCOLS={0}".format(columns))
    logger.info("XPOS={0}".format(xpos))
    # cols.refresh(0,
    #              xpos+5,
    #              0,
    #              0,
    #              1, columns-1)
    # Start printing text from (0,xpos) of the pad (first line, 3rd char)
    # on the screen at position (5,5)
    # with the maximum portion of the pad displayed being 20 chars x 15 lines
    # Since we only have one line, the 15 lines is overkill, but the 20 chars
    # will only show 20 characters before cutting off
    cols.refresh(0,
                 xpos+5,
                 0,
                 6,
                 1,
                 columns-1)
    # screen.refresh()
    # cols.refresh()
    return xpos+5

def scroll_left(logger, screen, cols, xpos, ypos):
    rows, columns = screen.getmaxyx()
    logger.info("NROWS={0}".format(rows))
    logger.info("NCOLS={0}".format(columns))
    logger.info("XPOS={0}".format(xpos))
    # cols.refresh(0,
    #              xpos+5,
    #              0,
    #              0,
    #              1, columns-1)
    # Start printing text from (0,xpos) of the pad (first line, 3rd char)
    # on the screen at position (5,5)
    # with the maximum portion of the pad displayed being 20 chars x 15 lines
    # Since we only have one line, the 15 lines is overkill, but the 20 chars
    # will only show 20 characters before cutting off
    cols.refresh(0,
                 xpos-5,
                 0,
                 6,
                 1,
                 columns-1)
    # screen.refresh()
    # cols.refresh()
    return xpos-5


def init_logger():
    """
    
    """
    logger = logging.getLogger(__file__)
    hdlr = logging.FileHandler(__file__ + ".log")
    formatter = logging.Formatter('%(asctime)s %(levelname)s %(message)s')
    hdlr.setFormatter(formatter)
    logger.addHandler(hdlr)
    logger.setLevel(logging.DEBUG)
    logger.info("******* Start ********")
    return logger


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def main(screen):
    screen.clear()
    screen.keypad(True)
    screen.scrollok(True)
    curses.use_default_colors()
    curses.noecho()

    logger = init_logger()
    rows, cols, data = create_layout(screen)
    # row_pad, col_pad, data_pad = create_pads(rows, cols, data)
    add_columns(screen, cols)
    add_rows(screen, rows)
    xpos = 0
    ypos = 0
    
    while True:
        ch = screen.getch()
        strch = chr(ch)

        if ch == curses.KEY_DOWN:
            logger.info('SCROLL DOWN')
            ypos = scroll_down(logger, screen, rows, xpos, ypos)
            # ren.scroll_down(nlines=nlines)
            # elif ch == curses.KEY_UP and mypad_pos > -2:
        elif ch == curses.KEY_UP:
            ypos = scroll_up(logger, screen, rows, xpos, ypos)            # ren.scroll_up(nlines=nlines)
        elif ch == curses.KEY_RIGHT:
            logger.info('SCROLL RIGHT')
            xpos = scroll_right(logger, screen, cols, xpos, ypos)
        elif ch == curses.KEY_LEFT:
            xpos = scroll_left(logger, screen, cols, xpos, ypos)
        elif strch == 'q':
            break
            # ren.scroll_left(nlines=nlines)


if __name__ == "__main__":
    curses.wrapper(main)
    
