#!/usr/bin/env python
import pprint as pp
import os

small_numbers = {1: "One", 2: "Two", 3: "Three", 4: "Four", 5: "Five", 6: "Six",
                 7: "Seven", 8: "Eight", 9: "Nine", 10: "Ten", 11: "Eleven",
                 12: "Twelve", 13: "Thirteen", 14: "Fourteen", 15: "Fifteen",
                 16: "Sixteen", 17: "Seventeen", 18: "Eighteen",
                 19: "Nineteen", 20: "Twenty", 30: "Thirty", 40: "Forty",
                 50: "Fifty", 60: "Sixty", 70: "Seventy",
                 80: "Eighty", 90: "Ninty"}
big_numbers = {100: "Hundred", 1000: "Thousand", 1000000: "Million",
               1000000000: "Billion", 1000000000000: "Trillion"}


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def get_text_number(number, loop=0):
    """
    Return a string based number for an int
    """
    try:
        # First we see if it is a match to one of the small numbers
        return small_numbers[number]
    except KeyError:
        # print("KEY_ERROR PASSED {0}={1}".format(loop, number))
        # This will hold the final parsed number for this function call
        final_number = ""

        # Convert the number into a string
        string_number = str(number)

        # If the number is greater than two digits then we will need an And
        # joiner at the end otherwise we do not
        joiner = ""
        if len(string_number) > 2:
            joiner = "And"

        # Grab the tens and units as they will need processing separately
        tens_and_units = string_number[len(string_number) -2:len(string_number)]

        try:
            # Make sure that we are not in the numbers as we mayb be
            tens = small_numbers[int(tens_and_units)]
        except KeyError:
            # No we are not, so get the number for the units and the tens
            # separately, this should work
            try:
                units = small_numbers[int(tens_and_units[1:2])]
                tens = "{0}{1}".format(small_numbers[int(tens_and_units[0:1])*10],
                                       units)
            except (ValueError, KeyError):
                joiner = ""
                tens = ""

        # Now we will iterate through the number that is cast to a string and
        # slice substrings out based on naming transitions in the number series
        # Store the upper range limit of the string
        strlen = len(string_number) - 1

        # These are the transitions, if a number is 1234567. 67 has been
        # processed above and th eremaining transitions:
        # So the first iteration will select 3,4 so the hundred digits, 5
        # So the next iteration is the thousands 4, 7, so the thousands 234
        # So the next iteration is the millions 7, 10, so 1
        transitions = [3, 4, 7, 10, 13, 16]
        for i in range(len(transitions)):
            # Our transitions are numbered from the right and are one based
            # We need to extract from the left and be 0 based. As we go through
            # the transitions it is possible that the number sits in the middle
            # of a transition, in which case the start will be negative and the
            # end will be positive, in these cases, we fix to the start of the
            # string (usingn max)
            try:
                start = max(0, strlen - (transitions[i+1] - 1) + 1)
                end = strlen - (transitions[i] - 1) + 1
            except IndexError:
                start = 0
                end = 0

            # If the end is every < 0 then we have overshot the string and we
            # break
            if end <= 0:
                break

            # Now extract the digits
            digit = string_number[start:end]

            # This gets the power of the digits e.g. Hundred, Thousand, Million
            power = big_numbers[10**(transitions[i] - 1)]

            remainder = get_text_number(digit, loop=loop+1)
            if remainder == "":
                power = "" 

            final_number = "{0}{1}{2}".format(remainder, power, final_number)
    return "{0}{1}{2}".format(final_number, joiner, tens)


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def get_test_matrix(nrows, ncols):
    """
    Generate a test matrix with nrows and ncols. This has numbers written as
    test
    """
    colnames = [get_text_number(i+1) for i in range(ncols)]
    # print(colnames)
    rownames = [get_text_number(i+1) for i in range(nrows)]
    # print(rownames)

    matrix = [[''] + colnames]
    for r in rownames:
        matrix.append([r] + ['{0}|{1}'.format(r, c) for c in colnames])

    return matrix


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def write_matrix(fn, matrix):
    with open(fn, 'w') as outfile:
        for i in matrix:
            outfile.write('{0}\n'.format("\t".join(i)))


if __name__ == '__main__':
    matrix = get_test_matrix(1000, 100)
    # pp.pprint(matrix)
    outfile = os.path.abspath('../test_data.txt')
    write_matrix(outfile, matrix)
